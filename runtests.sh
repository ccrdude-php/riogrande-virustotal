#!/bin/bash
if [ ! -f ./vendor/bin/phpunit ]
then
        composer update
fi

if [ -f ./vendor/bin/phpunit ]
then    
        ./vendor/bin/phpunit ./tests  --colors auto --display-warnings --testdox --bootstrap tests/bootstrap.php --filter testBadApiKeyException tests/APIv3/Domain/VotesTest.php
else
        if [ ! -f ./tests/phpunit-10.phar ]
        then
                curl -L https://phar.phpunit.de/phpunit-10.phar --output ./tests/phpunit-10.phar
        fi
        # probably need to add a stub that loads phpunit first
        php tests/phpunit-10.phar ./tests  --colors auto --testdox
fi
