#!/bin/bash
if [ ! -f ./tools/phpDocumentor.phar ]
then
    curl -L https://phpdoc.org/phpDocumentor.phar --output ./tools/phpDocumentor.phar
fi
php ./tools/phpDocumentor.phar -d source/ -t doc/
