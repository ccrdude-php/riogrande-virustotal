# RioGrande VirusTotal API

## VirusTotal

[VirusTotal](https://www.virustotal.com/) is a file (and more) scanning 
service that offers a 
[well documented](https://developers.virustotal.com/reference/overview/) 
public API to upload files and access information.

## RioGrande

RioGrande is simply the [top namespace](https://gitlab.com/ccrdude-php) 
I've picked for any PHP code I publish. It's a nerdy reference to a 
Star Trek shuttle - my top namespaces are named after spaceships.

## Abstract

Basic API v3 functionality to deal with files, IPs and domains,
retrieving reports, comments, votes (and adding them), behaviours...

For all requests, the returned JSON data can be accessed through a
PHP array. For important fields, getters are getting implemented.

[Create a new issue](/ccrdude-php/riogrande-virustotal/-/issues/new)
to request new features.

This is work in progress, trickling a bit slow since the code tests
easily reach the daily API limit of 500 calls currently.

More v3 functionality will be added over time.

## How to test the code

1. Check out this repository.
2. Run `composer update` to get development-related tools like
   phpunit and php-cs-fixer.
3. Open a console and run `php test.php`.
4. Open a console and run `./runtests.sh` to use phpunit.

## How to add to your own code

1. Download a .phar or create one using `./make-phar.sh`.
2. Copy `riogrande-virustotal-(version).phar` into your code folder.
3. Require said file using `require_once 'riogrande-virustotal-(version).phar';`
4. Try e.g. using the code below. 

```
require_once 'riogrande-virustotal-(version).phar';

$sResource = '8c8fbcf80f0484b48a07bd20e512b103969992dbf81b6588832b08205e3a1b43';
$report = new \RioGrande\VirusTotal\APIv3\FileBehaviours($sResource);
$report->setAPIKey('caffee...');
$report->execute();
print_r($report->getUninstallValues());
```

Or use the meta-class combining multiple requests:
```
require_once 'riogrande-virustotal-(version).phar';

$sResource = '8c8fbcf80f0484b48a07bd20e512b103969992dbf81b6588832b08205e3a1b43';
$file = new \RioGrande\VirusTotal\APIv3\VTFile($sResource);
$file->setAPIKey('caffee...');
$behaviours = $file->getBehaviours();
print_r($behaviours->getUninstallValues());
```

## Supported Universal API Endpoints

## Files

- [ ] Upload a file
- [ ] Get a URL for uploading large files
- [x] Get a file report
- [ ] Request a file rescan (re-analyze)
- [x] Get comments on a file
- [x] Add a comment to a file
- [x] Get votes on a file
- [x] Add a vote on a file
- [ ] Get objects related to a file
- [ ] Get object descriptors related to a file
- [x] Get a summary of all behavior reports for a file
- [x] Get a summary of all MITRE ATT&CK techniques observed in a file
- [x] Get all behavior reports for a file
- [ ] Get a file behavior report from a sandbox
- [ ] Get objects related to a behaviour report
- [ ] Get object descriptors related to a behaviour report
- [ ] Get a detailed HTML behaviour report
- [ ] Get a crowdsourced Sigma rule object
- [ ] Get a crowdsourced YARA ruleset

## URLs

- [ ] Scan URL
- [ ] Get a URL analysis report
- [ ] Request a URL rescan (re-analyze)
- [ ] Get comments on a URL
- [ ] Add a comment on a URL
- [ ] Get votes on a URL
- [ ] Add a vote on a URL
- [ ] Get objects related to a URL
- [ ] Get object descriptors related to a URL

## Domains & Resolutions

- [x] [Get a domain report](https://developers.virustotal.com/reference/domain-info)
- [x] [Get comments on a domain](https://developers.virustotal.com/reference/domains-comments-get)
- [x] [Add a comment to a domain](https://developers.virustotal.com/reference/domains-comments-post)
- [x] [Get votes on a domain](https://developers.virustotal.com/reference/domains-votes-get)
- [x] [Add a vote to a domain](https://developers.virustotal.com/reference/domain-votes-post)
- [x] [Get objects related to a domain](https://developers.virustotal.com/reference/domains-relationships)
- [x] [Get object descriptors related to a domain](https://developers.virustotal.com/reference/domains-relationships-ids)
- [x] [Get a DNS resolution object](https://developers.virustotal.com/reference/get-resolution-by-id)

## IP addresses

- [x] Get an IP address report
- [x] Get comments on an IP address
- [x] Add a comment to an IP addressp
- [x] Get votes on an IP address
- [x] Add a vote to an IP address
- [x] Get objects related to an IP address
- [x] Get object descriptors related to an IP address

## Analyses, Submissions & Operations

- [ ] Get a URL/file analysis
- [ ] Get objects related to an analysis
- [ ] Get object descriptors related to an analysis
- [ ] Get a submission object
- [ ] Get an operation object

## Graphs

- [ ] Search graphs
- [ ] Create a graph
- [ ] Get a graph object
- [ ] Update a graph object
- [ ] Delete a graph
- [ ] Get comments on a graph
- [ ] Add a comment to a graph
- [ ] Get users and groups that can view a graph
- [ ] Grant users and groups permission to see a graph
- [ ] Check if a user or group can view a graph
- [ ] Revoke view permission from a user or group
- [ ] Get users and groups that can edit a graph
- [ ] Grant users and groups permission to edit a graph
- [ ] Check if a user or group can edit a graph
- [ ] Revoke edit graph permissions from a user or group
- [ ] Get objects related to a graph
- [ ] Get object descriptors related to a graph

## Comments

- [ ] Get latest comments
- [ ] Get a comment object
- [ ] Update a comment
- [ ] Delete a comment
- [ ] Add a vote to a comment
- [ ] Get objects related to a comment
- [ ] Get object descriptors related to a comment

## Search & Metadata

- [ ] Search files, URLs, domains, IPs and tag comments
- [ ] Get VirusTotal metadata

## Collections

- [ ] Create a new collection
- [ ] Get a collection
- [ ] Update a collection
- [ ] Delete a collection
- [ ] Get comments on a collection
- [ ] Add a comment to a collection
- [ ] Get objects related to a collection
- [ ] Get object descriptors related to a collection
- [ ] Add new items to a collection
- [ ] Delete items from a collection

## Attack Tactics

- [ ] Get an attack tactic object
- [ ] Get objects related to an attack tactic
- [ ] Get object descriptors related to an attack tactic

## Attack Techniques

- [ ] Get an attack technique object
- [ ] Get objects related to an attack technique
- [ ] Get object descriptors related to an attack technique

## Popular Threat Categories

- [ ] Get a list of popular threat categories
