<?php

/**
 * API testing
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      1.0.0
 */

declare(strict_types=1);

namespace RioGrande\VirusTotal\APIv3;

use PHPUnit\Framework\TestCase;

/**
 * API testing
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      1.0.0
 */
final class FileBehaviourSummaryTest extends TestCase
{
    /**
     * Triggers an exception and finds it.
     *
     * @return void
     */
    public function testBadApiKeyException(): void
    {
        sleep(intval($GLOBALS['vtdelay']));
        $sResource
            = '8c8fbcf80f0484b48a07bd20e512b103969992dbf81b6588832b08205e3a1b43';
        $sAPIKey = 'caffee';
        $oBehaviourSummary = new FileBehaviourSummary($sResource);
        $oBehaviourSummary->setAPIKey($sAPIKey);
        $this->expectException(WrongCredentialsException::class);
        $oBehaviourSummary->execute();
        $this->assertFalse($oBehaviourSummary->execute());
    }
}
