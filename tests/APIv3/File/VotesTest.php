<?php

/**
 * API testing
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      1.0.0
 */

declare(strict_types=1);

namespace RioGrande\VirusTotal\APIv3\File;

use PHPUnit\Framework\TestCase;
use RioGrande\VirusTotal\APIv3\WrongCredentialsException;


/**
 * API testing
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      1.0.0
 */
final class VotesTest extends TestCase
{
    /**
     * Makes a call with a bad API key and expecting an exception.
     *
     * @return void
     */
    public function testBadApiKeyException(): void
    {
        sleep(intval($GLOBALS['vtdelay']));
        $sResource
            = '8c8fbcf80f0484b48a07bd20e512b103969992dbf81b6588832b08205e3a1b43';
        $sAPIKey = 'caffee';
        $oVotes = new Votes($sResource);
        $oVotes->setAPIKey($sAPIKey);
        $this->expectException(WrongCredentialsException::class);
        $this->assertFalse($oVotes->execute());
    }

    /**
     * Tests the number of votes associated with this file.
     *
     * @return void
     */
    public function testVotesCount(): void
    {
        $oConfig = $GLOBALS['vtconfig'];
        sleep(intval($GLOBALS['vtdelay']));
        // aCropAlyzer-0.1.zip
        $sResource
            = 'd2107af2f820aed26d9477a0688ed8510d88ae47e4cd94e9c5b72f7d45b66aef';
        $oVotes = new Votes($sResource);
        $oVotes->setAPIKey($oConfig->getAPIKey());
        $this->assertTrue($oVotes->execute());
        $this->assertEquals(1, $oVotes->getVoteCount());
    }
}
