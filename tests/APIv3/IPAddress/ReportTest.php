<?php

/**
 * API testing
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      1.0.0
 */

declare(strict_types=1);

namespace RioGrande\VirusTotal\APIv3\IPAddress;

use PHPUnit\Framework\TestCase;
use RioGrande\VirusTotal\APIv3\WrongCredentialsException;

/**
 * API testing
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      1.0.0
 */
final class ReportTest extends TestCase
{
    /**
     * Makes a call with a bad API key and expecting an exception.
     *
     * @return void
     */
    public function testBadApiKey(): void
    {
        sleep(intval($GLOBALS['vtdelay']));
        $sAPIKey = 'caffee';
        $oReport = new Report('144.76.80.198');
        $oReport->setAPIKey($sAPIKey);
        $this->expectException(WrongCredentialsException::class);
        $this->assertFalse($oReport->execute());
    }

    /**
     * Tests the registrar of the tested domain.
     *
     * @return void
     */
    public function testASOwner(): void
    {
        $oConfig = $GLOBALS['vtconfig'];
        sleep(intval($GLOBALS['vtdelay']));
        $oReport = new Report('144.76.80.198');
        $oReport->setAPIKey($oConfig->getAPIKey());
        $this->assertTrue($oReport->execute());
        $this->assertEquals('Hetzner Online GmbH', $oReport->getASOwner());
    }
}
