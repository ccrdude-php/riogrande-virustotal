<?php

/**
 * API testing
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      1.0.0
 */

declare(strict_types=1);

namespace RioGrande\VirusTotal\APIv3\IPAddress;

use PHPUnit\Framework\TestCase;
use RioGrande\VirusTotal\APIv3\WrongCredentialsException;

/**
 * API testing
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      1.0.0
 */
final class ObjectDescriptorsTest extends TestCase
{
    /**
     * Makes a call with a bad API key and expecting an exception.
     *
     * @return void
     */
    public function testBadApiKeyException(): void
    {
        sleep(intval($GLOBALS['vtdelay']));
        $sAPIKey = 'caffee';
        $oComments = new ObjectDescriptors('144.76.80.198', 'comments');
        $oComments->setAPIKey($sAPIKey);
        $this->expectException(WrongCredentialsException::class);
        $this->assertFalse($oComments->execute());
    }

    /**
     * Tests the number of comments associated with the domain.
     *
     * @return void
     */
    public function testCommentCount(): void
    {
        $oConfig = $GLOBALS['vtconfig'];
        sleep(intval($GLOBALS['vtdelay']));
        $oComments = new ObjectDescriptors('144.76.80.198', 'comments');
        $oComments->setAPIKey($oConfig->getAPIKey());
        $this->assertTrue($oComments->execute());
        $this->assertEquals(2, $oComments->getObjectDescriptorCount());
    }
}
