<?php

/**
 * API testing
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      1.0.0
 */

declare(strict_types=1);

namespace RioGrande\VirusTotal\APIv3;

use PHPUnit\Framework\TestCase;

/**
 * API testing
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      1.0.0
 */
final class VTFileTest extends TestCase
{
    /**
     * Makes a call with a bad API key and expecting an exception.
     *
     * @return void
     */
    public function testBadApiKeyException(): void
    {
        sleep(intval($GLOBALS['vtdelay']));
        $sResource
            = 'd2107af2f820aed26d9477a0688ed8510d88ae47e4cd94e9c5b72f7d45b66aef';
        $sAPIKey = 'caffee';
        $file = new VTFile($sResource);
        $file->setAPIKey($sAPIKey);
        $this->expectException(WrongCredentialsException::class);
        $file->getReport();
    }

    public function testGetReport(): void
    {
        sleep(intval($GLOBALS['vtdelay']));
        $oConfig = $GLOBALS['vtconfig'];
        $sResource
            = 'd2107af2f820aed26d9477a0688ed8510d88ae47e4cd94e9c5b72f7d45b66aef';
        $file = new VTFile($sResource);
        $file->setAPIKey($oConfig->getAPIKey());
        $report = $file->getReport();
        $this->assertEquals('zip', $report->getFileTypeExtension());
        $this->assertEquals('ZIP', $report->getFileTypeDescription());
    }

    public function testGetBehaviours(): void
    {
        sleep(intval($GLOBALS['vtdelay']));
        $oConfig = $GLOBALS['vtconfig'];
        $sResource
            = 'f004c98f0011b2289d929448bad26a49664d51143bf8fb949ae1c5645d716aed';
        $oFile = new VTFile($sResource);
        $oFile->setAPIKey($oConfig->getAPIKey());
        $oBehaviours = $oFile->getBehaviours();
        $aValues = $oBehaviours->getUninstallValues();
        $this->assertEquals(1, count($aValues));
        $this->assertEquals('CrystalDiskInfo_is1', array_keys($aValues)[0]);
    }

    public function testGetBehaviourSummary(): void
    {
        sleep(intval($GLOBALS['vtdelay']));
        $oConfig = $GLOBALS['vtconfig'];
        $sResource
            = 'f004c98f0011b2289d929448bad26a49664d51143bf8fb949ae1c5645d716aed';
        $oFile = new VTFile($sResource);
        $oFile->setAPIKey($oConfig->getAPIKey());
        $oBehaviourSummary = $oFile->getBehaviourSummary();
        $aValues = $oBehaviourSummary->getUninstallValues();
        $this->assertEquals(1, count($aValues));
        $this->assertEquals('CrystalDiskInfo_is1', array_keys($aValues)[0]);
    }

    public function testGetComments(): void
    {
        sleep(intval($GLOBALS['vtdelay']));
        $oConfig = $GLOBALS['vtconfig'];
        $sResource
            = 'd2107af2f820aed26d9477a0688ed8510d88ae47e4cd94e9c5b72f7d45b66aef';
        $file = new VTFile($sResource);
        $file->setAPIKey($oConfig->getAPIKey());
        $comments = $file->getComments();
        $this->assertEquals(0, $comments->getCommentCount());
    }

    public function testGetVotes(): void
    {
        sleep(intval($GLOBALS['vtdelay']));
        $oConfig = $GLOBALS['vtconfig'];
        $sResource
            = 'd2107af2f820aed26d9477a0688ed8510d88ae47e4cd94e9c5b72f7d45b66aef';
        $file = new VTFile($sResource);
        $file->setAPIKey($oConfig->getAPIKey());
        $votes = $file->getVotes();
        $this->assertEquals(1, $votes->getVoteCount());
    }

    public function testAddVote(): void
    {
        sleep(intval($GLOBALS['vtdelay']));
        $oConfig = $GLOBALS['vtconfig'];
        $sResource
            = 'd2107af2f820aed26d9477a0688ed8510d88ae47e4cd94e9c5b72f7d45b66aef';
        $file = new VTFile($sResource);
        $file->setAPIKey($oConfig->getAPIKey());
        $this->expectException(AlreadyExistsException::class);
        $file->addVote(false);
    }
}
