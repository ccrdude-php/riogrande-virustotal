<?php

/**
 * API testing
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      1.0.0
 */

declare(strict_types=1);

namespace RioGrande\VirusTotal\APIv3\Domain;

use PHPUnit\Framework\TestCase;
use RioGrande\VirusTotal\APIv3\WrongCredentialsException;

/**
 * API testing
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      1.0.0
 */
final class VotesTest extends TestCase
{
    /**
     * Makes a call with a bad API key and expecting an exception.
     *
     * @return void
     */
    public function testBadApiKeyException(): void
    {
        sleep(intval($GLOBALS['vtdelay']));
        $sAPIKey = 'caffee';
        $oVotes = new Votes('ccrdude.net');
        $oVotes->setAPIKey($sAPIKey);
        $this->expectException(WrongCredentialsException::class);
        $this->assertFalse($oVotes->execute());
    }

    /**
     * Tests for the nimber of votes received.
     *
     * @return void
     */
    public function testVoteCount(): void
    {
        $oConfig = $GLOBALS['vtconfig'];
        sleep(intval($GLOBALS['vtdelay']));
        $oVotes = new Votes('ccrdude.net');
        $oVotes->setAPIKey($oConfig->getAPIKey());
        $this->assertTrue($oVotes->execute());
        $this->assertEquals(1, $oVotes->getVoteCount());
    }

    /**
     * Tests if the votes label this file harmless.
     *
     * @return void
     */
    public function testIsVoteHarmless(): void
    {
        $oConfig = $GLOBALS['vtconfig'];
        sleep(intval($GLOBALS['vtdelay']));
        $oVotes = new Votes('ccrdude.net');
        $oVotes->setAPIKey($oConfig->getAPIKey());
        $this->assertTrue($oVotes->execute());
        $this->assertTrue($oVotes->isVerdictHarmless(0));
        $this->assertFalse($oVotes->isVerdictMalicious(0));
    }
}
