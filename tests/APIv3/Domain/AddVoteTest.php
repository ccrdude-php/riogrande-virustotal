<?php

/**
 * API testing
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      1.0.0
 */

declare(strict_types=1);

namespace RioGrande\VirusTotal\APIv3\Domain;

use PHPUnit\Framework\TestCase;
use RioGrande\VirusTotal\APIv3\{WrongCredentialsException, AlreadyExistsException};

/**
 * API testing
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      1.0.0
 */
final class AddVoteTest extends TestCase
{
    /**
     * Tests using a bad API key, expecting an exception.
     *
     * @return void
     */
    public function testBadApiKeyException(): void
    {
        sleep(intval($GLOBALS['vtdelay']));
        $sAPIKey = 'caffee';

        $oAddVote = new AddVote('ccrdude.net', false);
        $oAddVote->setAPIKey($sAPIKey);
        $this->expectException(WrongCredentialsException::class);
        $this->assertFalse($oAddVote->execute());
    }

    /**
     * Tests for a domain that we have already voted on.
     * If this test fails, run the test again, this time the vote will be
     * existing.
     *
     * @return void
     */
    public function testVoteAlreadyExists(): void
    {
        $oConfig = $GLOBALS['vtconfig'];
        sleep(intval($GLOBALS['vtdelay']));
        $oAddVote = new AddVote('ccrdude.net', false);
        $oAddVote->setAPIKey($oConfig->getAPIKey());
        $this->expectException(AlreadyExistsException::class);
        $this->assertTrue($oAddVote->execute());
    }
}
