<?php

/**
 * API testing
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      1.0.0
 */

declare(strict_types=1);

namespace RioGrande\VirusTotal\APIv3;

use PHPUnit\Framework\TestCase;

/**
 * API testing
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      1.0.0
 */
final class VTIPAddressTest extends TestCase
{
    /**
     * Makes a call with a bad API key and expecting an exception.
     *
     * @return void
     */
    public function testBadApiKeyException(): void
    {
        sleep(intval($GLOBALS['vtdelay']));
        $sAPIKey = 'caffee';
        $file = new VTIPAddress('144.76.80.198');
        $file->setAPIKey($sAPIKey);
        $this->expectException(WrongCredentialsException::class);
        $file->getReport();
    }

    public function testGetReport(): void
    {
        sleep(intval($GLOBALS['vtdelay']));
        $oConfig = $GLOBALS['vtconfig'];
        $oIPAddress = new VTIPAddress('144.76.80.198');
        $oIPAddress->setAPIKey($oConfig->getAPIKey());
        $oReport = $oIPAddress->getReport();
        $this->assertEquals('Hetzner Online GmbH', $oReport->getASOwner());
    }

    public function testGetComments(): void
    {
        sleep(intval($GLOBALS['vtdelay']));
        $oConfig = $GLOBALS['vtconfig'];
        $oIPAddress = new VTIPAddress('144.76.80.198');
        $oIPAddress->setAPIKey($oConfig->getAPIKey());
        $oComments = $oIPAddress->getComments();
        $this->assertEquals(2, $oComments->getCommentCount());
    }

    public function testGetVotes(): void
    {
        sleep(intval($GLOBALS['vtdelay']));
        $oConfig = $GLOBALS['vtconfig'];
        $oIPAddress = new VTIPAddress('144.76.80.198');
        $oIPAddress->setAPIKey($oConfig->getAPIKey());
        $oVotes = $oIPAddress->getVotes();
        $this->assertEquals(1, $oVotes->getVoteCount());
    }

    public function testGetObjects(): void
    {
        sleep(intval($GLOBALS['vtdelay']));
        $oConfig = $GLOBALS['vtconfig'];
        $oIPAddress = new VTIPAddress('144.76.80.198');
        $oIPAddress->setAPIKey($oConfig->getAPIKey());
        $oComments = $oIPAddress->getObjects('comments');
        $this->assertEquals(2, $oComments->getObjectCount());
    }

    public function testGetObjectDescriptors(): void
    {
        sleep(intval($GLOBALS['vtdelay']));
        $oConfig = $GLOBALS['vtconfig'];
        $oIPAddress = new VTIPAddress('144.76.80.198');
        $oIPAddress->setAPIKey($oConfig->getAPIKey());
        $oComments = $oIPAddress->getObjectDescriptors('comments');
        $this->assertEquals(2, $oComments->getObjectDescriptorCount());
    }

    public function testAddVote(): void
    {
        sleep(intval($GLOBALS['vtdelay']));
        $oConfig = $GLOBALS['vtconfig'];
        $oIPAddress = new VTIPAddress('144.76.80.198');
        $oIPAddress->setAPIKey($oConfig->getAPIKey());
        $this->expectException(AlreadyExistsException::class);
        $oIPAddress->addVote(false);
    }

    public function testAddComment(): void
    {
        sleep(intval($GLOBALS['vtdelay']));
        $oConfig = $GLOBALS['vtconfig'];
        $oIPAddress = new VTIPAddress('144.76.80.198');
        $oIPAddress->setAPIKey($oConfig->getAPIKey());
        $this->expectException(AlreadyExistsException::class);
        $oIPAddress->addComment('Server owned by software developer CCRDude');
    }
}