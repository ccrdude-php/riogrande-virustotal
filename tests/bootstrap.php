<?php

namespace RioGrande\VirusTotal\APIv3;

require_once __DIR__ . '/../source/load.php';

$oConfig = new Config(__DIR__ . '/../config.json');
$oConfig->requestRequiredCredentialsOnCommandLine();
$GLOBALS['vtconfig'] = $oConfig;
$GLOBALS['vtdelay'] = 1;