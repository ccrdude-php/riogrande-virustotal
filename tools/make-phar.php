<?php

/**
 * Creates a phar archive to simplify distribution and use.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */

try {
    $sFilename = './tools/riogrande-virustotal-0.1.3.phar';
    if (file_exists($sFilename)) {
        unlink($sFilename);
    }
    if (file_exists($sFilename . '.gz')) {
        unlink($sFilename . '.gz');
    }

    $phar = new Phar($sFilename);
    $phar->startBuffering();
    $defaultStub = $phar->createDefaultStub('load.php');
    $phar->buildFromDirectory(__DIR__ . '/../source');
    $phar->setStub($defaultStub);
    $phar->stopBuffering();

    // plus - compressing it into gzip
    $phar->compressFiles(Phar::GZ);

    // Make the file executable
    chmod(__DIR__ . "/../{$sFilename}", 0770);

    echo "$sFilename successfully created" . PHP_EOL;
} catch (Exception $e) {
    echo $e->getMessage();
}
