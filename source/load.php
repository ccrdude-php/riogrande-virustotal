<?php

/**
 * Loads all required files.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/virustotal
 * @since      0.1.2
 */

namespace RioGrande\VirusTotal;

require_once __DIR__ . '/APIv3/Exceptions/load.php';
require_once __DIR__ . '/APIv3/Config.php';
require_once __DIR__ . '/APIv3/Query.php';
require_once __DIR__ . '/APIv3/Request.php';
require_once __DIR__ . '/APIv3/RequestAddComment.php';
require_once __DIR__ . '/APIv3/RequestAddVote.php';
require_once __DIR__ . '/APIv3/RequestComments.php';
require_once __DIR__ . '/APIv3/RequestVotes.php';
require_once __DIR__ . '/APIv3/RequestReport.php';
require_once __DIR__ . '/APIv3/Domain/load.php';
require_once __DIR__ . '/APIv3/File/load.php';
require_once __DIR__ . '/APIv3/IPAddress/load.php';
require_once __DIR__ . '/APIv3/VTDomain.php';
require_once __DIR__ . '/APIv3/VTFile.php';
require_once __DIR__ . '/APIv3/VTIPAddress.php';
