<?php

/**
 * Adds a vote for a domain.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */

namespace RioGrande\VirusTotal\APIv3;

use RioGrande\VirusTotal\APIv3\Request;

/**
 * Adds a vote for a domain.
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @see        https://developers.virustotal.com/reference/domain-info
 * @see        https://developers.virustotal.com/reference/domains-1
 * @since      0.1.2
 */
class RequestAddVote extends Request
{
    protected string $Path;
    protected string $Data;
    protected bool $VerdictMalicious;

    /**
     * Initializes an AddVote request.
     *
     * @param string $Path         The path to add comments in.
     * @param string $Data         The data to add comments for.
     * @param bool   $IsMalicious Whether the file is to be voted as malicious.
     *
     * @author Patrick Kolla-ten Venne
     */
    public function __construct(string $Path, string $Data, bool $IsMalicious)
    {
        $this->Path = $Path;
        $this->Data = $Data;
        $this->VerdictMalicious = $IsMalicious;
        $this->setURL("{$this->APIBase}/{$this->Path}/{$this->Data}/votes");
    }

    /**
     * Executes the AddVote request.
     *
     * @return bool
     *
     * @author Patrick Kolla-ten Venne
     */
    public function execute(): bool
    {
        $this->Query->setHeaders(["x-apikey: {$this->APIKey}"]);
        $sVerdict = ($this->VerdictMalicious ? 'malicious' : 'harmless');
        $a = array(
            'data' => array(
                'type' => 'vote',
                'attributes' => array(
                    'verdict' => $sVerdict,
                ),
            ),
        );
        $this->Query->setPostData(json_encode($a));
        $this->QueryResponse = $this->Query->execute();
        if (is_null($this->QueryResponse)) {
            return false;
        }
        $ret = $this->QueryResponse;
        return true;
    }
}
