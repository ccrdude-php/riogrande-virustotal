<?php

/**
 * Implements a query to REST APIs, using CURL.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/virustotal
 * @since      0.1.2
 */

namespace RioGrande\VirusTotal\APIv3;

/**
 * Implements a query to REST APIs, using CURL.
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/virustotal
 * @since      0.1.2
 */
class Query
{
    protected string $FURL;
    protected bool $FDoPost = false;
    protected array $FHeaders = [];
    protected string|array $FPostData = [];
    protected string $FCURLError = '';
    protected array $FDebugOutput = [];

    /**
     * Prepares the query with the URL to use.
     *
     * @param string $TheURL The URL to send the query to.
     */
    public function __construct(string $TheURL)
    {
        $this->FURL = $TheURL;
    }

    /**
     * Executes a query, returning null if it fails, or JSON data otherwises.
     *
     * @return array|null
     */
    public function execute(): ?array
    {
        $this->FDebugOutput['url'] = $this->FURL;
        $this->FDebugOutput['type'] = $this->FDoPost ? 'POST' : 'GET';
        $this->FDebugOutput['outgoing'] = array(
            'body' => $this->FPostData,
            'headers' => $this->FHeaders,
        );
        $ch = curl_init($this->FURL);
        try {
            curl_setopt($ch, CURLOPT_POST, $this->FDoPost);
            if ($this->FDoPost) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $this->FPostData);
            }
            curl_setopt($ch, CURLOPT_HTTPHEADER, $this->FHeaders);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            if ($response === false) {
                $this->FCURLError = curl_error($ch);
                $ret = null;
            }
            $responseData = json_decode($response, true);
            $ret = $responseData;
        } finally {
            curl_close($ch);
        }
        $this->FDebugOutput['incoming'] = array(
            'body' => $ret,
        );
        if (is_array($ret)) {
            if (array_key_exists('error', $ret)) {
                $aError = $ret['error'];
                if (array_key_exists('code', $aError)) {
                    switch ($aError['code']) {
                        case 'BadRequestError':
                            throw new BadRequestException($aError['message']);
                        case 'InvalidArgumentError':
                            throw new InvalidArgumentException($aError['message']);
                        case 'NotAvailableYet':
                            throw new NotAvailableYetException($aError['message']);
                        case 'UnselectiveContentQueryError':
                            throw new UnselectiveContentQueryException($aError['message']);
                        case 'UnsupportedContentQueryError':
                            throw new UnsupportedContentQueryException($aError['message']);
                        case 'AuthenticationRequiredError':
                            throw new AuthenticationRequiredException($aError['message']);
                        case 'UserNotActiveError':
                            throw new UserNotActiveException($aError['message']);
                        case 'WrongCredentialsError':
                            throw new WrongCredentialsException($aError['message']);
                        case 'ForbiddenError':
                            throw new ForbiddenException($aError['message']);
                        case 'NotFoundError':
                            throw new NotFoundException($aError['message']);
                        case 'AlreadyExistsError':
                            throw new AlreadyExistsException($aError['message']);
                        case 'FailedDependencyError':
                            throw new FailedDependencyException($aError['message']);
                        case 'QuotaExceededError':
                            throw new QuoteExceededException($aError['message']);
                        case 'TooManyRequestsError':
                            throw new TooManyRequestsException($aError['message']);
                        case 'TransientError':
                            throw new TransientException($aError['message']);
                        case 'DeadlineExceededError':
                            throw new DeadlineExceededException($aError['message']);
                        default:
                            throw new \Exception($aError['code'] . ': ' . $aError['message']);
                    }
                }
            }
        }
        return $ret;
    }

    /**
     * Returns debug information.
     *
     * @return array
     */
    public function getDebugOutput(): array
    {
        return $this->FDebugOutput;
    }

    /**
     * Returns a possible error text.
     *
     * @return string
     */
    public function getError(): string
    {
        return $this->FCURLError;
    }

    /**
     * Returns the URL associated with the query.
     *
     * @return string
     */
    public function getURL(): string
    {
        return $this->FURL;
    }

    /**
     * Sets headers to be used by the query.
     *
     * @param array $AHeaders A list of header entries of form "A: B".
     *
     * @return Query An instance of this to chain commands.
     */
    public function setHeaders(array $AHeaders): Query
    {
        $this->FHeaders = $AHeaders;
        return $this;
    }

    /**
     * Sets the data sent with a POST query. Changes query type to POST as well.
     *
     * @param string|array $APostData The parameters passed by POST.
     *
     * @return Query An instance of this to chain commands.
     */
    public function setPostData(string|array $APostData): Query
    {
        $this->FPostData = $APostData;
        $this->FDoPost = true;
        return $this;
    }
}
