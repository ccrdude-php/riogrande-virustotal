<?php

/**
 * Base class for all requests to the API.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/virustotal
 * @since      0.1.2
 */

namespace RioGrande\VirusTotal\APIv3;

use RioGrande\VirusTotal\APIv3\Query;

/**
 * Base class for all requests to the API.
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/virustotal
 * @since      0.1.2
 */
abstract class Request
{
    protected Query $Query;
    protected string $URL = '';
    protected string $APIKey;
    protected ?array $QueryResponse = null;
    public string $APIBase = 'https://www.virustotal.com/api/v3';

    /**
     * Set the value of APIKey.
     *
     * @param string $APIKey API key to pass no to VirusTotal.
     *
     * @return Request Instance of self to chain commands.
     */
    public function setAPIKey(string $APIKey): Request
    {
        $this->APIKey = $APIKey;
        return $this;
    }

    /**
     * Set the value of URL.
     *
     * @param string $URL URL to contact.
     *
     * @return Request Instance of self to chain commands.
     */
    public function setURL(string $URL): Request
    {
        $this->URL = $URL;
        unset($this->Query);
        $this->Query = new Query($URL);
        return $this;
    }

    /**
     * Get the query results.
     *
     * @return ?array
     */
    public function getQueryResponse(): ?array
    {
        return $this->QueryResponse;
    }

    public function getDebugOutput(): array
    {
        return $this->Query->getDebugOutput();
    }
}
