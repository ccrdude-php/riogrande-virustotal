<?php

/**
 * Adds a Comment for an IP address.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */

namespace RioGrande\VirusTotal\APIv3\IPAddress;

use RioGrande\VirusTotal\APIv3\RequestAddComment;

/**
 * Adds a Comment for an IP address.
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */
class AddComment extends RequestAddComment
{
   /**
     * Initializes a IPAddComment request.
     *
     * @param string $IPAddress The IP address for which the Comment is to be
     *                          added.
     * @param string $Comment   Whether the file is to be Commentd as malicious.
     *
     * @author Patrick Kolla-ten Venne
     */
    public function __construct(string $IPAddress, string $Comment)
    {
        parent::__construct('ip_addresses', $IPAddress, $Comment);
    }
}
