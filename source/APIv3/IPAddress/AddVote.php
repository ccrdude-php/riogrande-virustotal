<?php

/**
 * Adds a vote for an IP address.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */

namespace RioGrande\VirusTotal\APIv3\IPAddress;

use RioGrande\VirusTotal\APIv3\RequestAddVote;

/**
 * Adds a vote for an IP address.
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */
class AddVote extends RequestAddVote
{
    /**
     * Initializes a IPAddVote request.
     *
     * @param string $IPAddress   The IP address for which the vote is to be
     *                            added.
     * @param bool   $IsMalicious Whether the file is to be voted as malicious.
     *
     * @author Patrick Kolla-ten Venne
     */
    public function __construct(string $IPAddress, bool $IsMalicious)
    {
        parent::__construct('ip_addresses', $IPAddress, $IsMalicious);
    }
}
