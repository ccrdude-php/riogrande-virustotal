<?php

/**
 * Get object descriptors related to an IP address.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */

namespace RioGrande\VirusTotal\APIv3\IPAddress;

use RioGrande\VirusTotal\APIv3\Request;

/**
 * Get object descriptors related to an IP address.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @see        https://developers.virustotal.com/reference/ip-relationships-ids
 * @see        https://developers.virustotal.com/reference/ip-object
 * @since      0.1.2
 */
class ObjectDescriptors extends Request
{
    protected string $IPAddress;
    protected string $Relationship;
    protected int $ResultLimit;

    /**
     * Initializes the IPComments request.
     *
     * @param string $IPAddress    The IP address to query for.
     * @param string $Relationship The relationship name.
     * @param int    $AResultLimit Maximum number of results to query.
     *
     * @author Patrick Kolla-ten Venne
     */
    public function __construct(string $AnIPAddress, string $ARelationship, int $AResultLimit = 10)
    {
        $this->IPAddress = $AnIPAddress;
        $this->Relationship = $ARelationship;
        $this->ResultLimit = $AResultLimit;
        $sURL = $this->APIBase;
        $sURL .= "/ip_addresses/{$this->IPAddress}";
        $sURL .= "/relationships";
        $sURL .= "/{$this->Relationship}?limit={$this->ResultLimit}";
        $this->setURL($sURL);
    }

    /**
     * Executs the IPComments request.
     *
     * @return bool
     *
     * @author Patrick Kolla-ten Venne
     */
    public function execute(): bool
    {
        $this->Query->setHeaders(["x-apikey: {$this->APIKey}"]);
        $this->QueryResponse = $this->Query->execute();
        if (is_null($this->QueryResponse)) {
            return false;
        }
        return true;
    }

    /**
     * Returns the received number of object descriptors.
     *
     * @return int
     *
     * @author Patrick Kolla-ten Venne
     */
    public function getObjectDescriptorCount(): int
    {
        if (is_null($this->QueryResponse)) {
            throw new \Exception('Not a valid answer');
        }
        return intval($this->QueryResponse['meta']['count']);
    }
}
