<?php

/**
 * Get comments on an IP address.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */

namespace RioGrande\VirusTotal\APIv3\IPAddress;

use RioGrande\VirusTotal\APIv3\RequestComments;

/**
 * Get comments on an IP address.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @see        https://developers.virustotal.com/reference/ip-comments-get
 * @since      0.1.2
 */
class Comments extends RequestComments
{
    /**
     * Initializes the IPComments request.
     *
     * @param string $IPAddress    The IP address to query comments for.
     * @param int    $AResultLimit Maximum number of results to query.
     *
     * @author Patrick Kolla-ten Venne
     */
    public function __construct(string $IPAddress, int $AResultLimit = 10)
    {
        parent::__construct('ip_addresses', $IPAddress, $AResultLimit);
    }
}
