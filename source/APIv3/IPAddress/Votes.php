<?php

/**
 * Retrieves votes about an IP address.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */

namespace RioGrande\VirusTotal\APIv3\IPAddress;

use RioGrande\VirusTotal\APIv3\RequestVotes;

/**
 * Retrieves votes about an IP address.
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @see        https://developers.virustotal.com/reference/domain-info
 * @see        https://developers.virustotal.com/reference/domains-1
 * @since      0.1.2
 */
class Votes extends RequestVotes
{
    /**
     * Initializes a IPVotes request.
     *
     * @param string $IPAddress The IP address to query.
     *
     * @author Patrick Kolla-ten Venne
     */
    public function __construct(string $IPAddress)
    {
        parent::__construct('ip_addresses', $IPAddress);
    }
}
