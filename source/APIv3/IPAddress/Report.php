<?php

/**
 * Get an IP address report.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */

namespace RioGrande\VirusTotal\APIv3\IPAddress;

use RioGrande\VirusTotal\APIv3\RequestReport;

/**
 * Get an IP address report.
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @see        https://developers.virustotal.com/reference/ip-info
 * @see        https://developers.virustotal.com/reference/ip-object
 * @since      0.1.2
 */
class Report extends RequestReport
{
    /**
     * Initializes a IPReport request.
     *
     * @param string $IPAddress The domain to get a report about.
     *
     * @author Patrick Kolla-ten Venne
     */
    public function __construct(string $IPAddress)
    {
        parent::__construct('ip_addresses', $IPAddress);
    }

    /**
     * Returns the network of the IP after a successful request.
     *
     * @return string
     *
     * @author Patrick Kolla-ten Venne
     */
    public function getNetwork(): string
    {
        if (is_null($this->QueryResponse)) {
            throw new \Exception('Not a valid answer');
        }
        return $this->QueryResponse['data']['attributes']['network'];
    }

    /**
     * Returns the AS Owner of the IP after a successful request.
     *
     * @return string
     *
     * @author Patrick Kolla-ten Venne
     */
    public function getASOwner(): string
    {
        if (is_null($this->QueryResponse)) {
            throw new \Exception('Not a valid answer');
        }
        return $this->QueryResponse['data']['attributes']['as_owner'];
    }
}
