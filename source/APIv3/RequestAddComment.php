<?php

/**
 * Add comments.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */

namespace RioGrande\VirusTotal\APIv3;

use RioGrande\VirusTotal\APIv3\Request;

/**
 * Add comments.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @see        https://developers.virustotal.com/reference/domain-info
 * @see        https://developers.virustotal.com/reference/domains-1
 * @since      0.1.2
 */
class RequestAddComment extends Request
{
    protected string $Path;
    protected string $Data;
    protected string $Comment;
    protected int $ResultLimit;

    /**
     * Initializes the add comment request.
     *
     * @param string $Path         The path to add comments in.
     * @param string $Data         The data to add comments for.
     * @param string $Comment      Comment text.
     *
     * @author Patrick Kolla-ten Venne
     */
    public function __construct(string $Path, string $Data, string $Comment)
    {
        $this->Path = $Path;
        $this->Data = $Data;
        $this->Comment = $Comment;
        $sURL = $this->APIBase;
        $sURL .= "/{$this->Path}/{$this->Data}";
        $sURL .= "/comments";
        $this->setURL($sURL);
    }

    /**
     * Executs the add comment request.
     *
     * @return bool
     *
     * @author Patrick Kolla-ten Venne
     */
    public function execute(): bool
    {
        $this->Query->setHeaders(["x-apikey: {$this->APIKey}"]);
        $a = array(
            'data' => array(
                'type' => 'comment',
                'attributes' => array(
                    'text' => $this->Comment,
                ),
            ),
        );
        $this->Query->setPostData(json_encode($a));
        $this->QueryResponse = $this->Query->execute();
        if (is_null($this->QueryResponse)) {
            return false;
        }
        return true;
    }

    /**
     * Returns the received number of comments.
     *
     * @return int
     *
     * @author Patrick Kolla-ten Venne
     */
    public function getCommentCount(): int
    {
        if (is_null($this->QueryResponse)) {
            throw new \Exception('Not a valid answer');
        }
        return intval($this->QueryResponse['meta']['count']);
    }
}
