<?php

/**
 * Get objects related to an IP address.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */

namespace RioGrande\VirusTotal\APIv3\Domain;

use RioGrande\VirusTotal\APIv3\Request;

/**
 * Get objects related to an IP address.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @see        https://developers.virustotal.com/reference/get-resolution-by-id
 * @see        https://developers.virustotal.com/reference/resolution-object
 * @since      0.1.2
 */
class DNSResolutionObject extends Request
{
    protected string $IPAddress;
    protected string $Domain;
    protected int $ResultLimit;

    /**
     * Initializes the DNSResolutionObject request.
     *
     * @param string $IPAddress   The IP address to query for.
     * @param string $Domain       The IP address to query for.
     *
     * @author Patrick Kolla-ten Venne
     */
    public function __construct(string $IPAddress, string $Domain)
    {
        $this->IPAddress = $IPAddress;
        $this->Domain = $Domain;
        $sURL = $this->APIBase;
        $sURL .= "/resolutions/{$this->IPAddress}{$this->Domain}";
        $this->setURL($sURL);
    }

    /**
     * Executs the DNSResolutionObject request.
     *
     * @return bool
     *
     * @author Patrick Kolla-ten Venne
     */
    public function execute(): bool
    {
        $this->Query->setHeaders(["x-apikey: {$this->APIKey}"]);
        $this->QueryResponse = $this->Query->execute();
        if (is_null($this->QueryResponse)) {
            return false;
        }
        return true;
    }
}
