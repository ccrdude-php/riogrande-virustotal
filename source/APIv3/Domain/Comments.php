<?php

/**
 * Retrieves comments about a domain.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */

namespace RioGrande\VirusTotal\APIv3\Domain;

use RioGrande\VirusTotal\APIv3\RequestComments;

/**
 * Retrieves comments about a domain.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @see        https://developers.virustotal.com/reference/domain-info
 * @see        https://developers.virustotal.com/reference/domains-1
 * @since      0.1.2
 */
class Comments extends RequestComments
{
    /**
     * Initializes the DomainComments request.
     *
     * @param string $TheDomain    The domain to query comments for.
     * @param int    $AResultLimit Maximum number of results to query.
     *
     * @author Patrick Kolla-ten Venne
     */
    public function __construct(string $TheDomain, int $AResultLimit = 10)
    {
        parent::__construct('domains', $TheDomain, $AResultLimit);
    }
}
