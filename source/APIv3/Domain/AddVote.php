<?php

/**
 * Adds a vote for a domain.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */

namespace RioGrande\VirusTotal\APIv3\Domain;

use RioGrande\VirusTotal\APIv3\RequestAddVote;

/**
 * Adds a vote for a domain.
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @see        https://developers.virustotal.com/reference/domain-info
 * @see        https://developers.virustotal.com/reference/domains-1
 * @since      0.1.2
 */
class AddVote extends RequestAddVote
{
    /**
     * Initializes a DomainAddVote request.
     *
     * @param string $TheDomain   The domain for which the vote is to be added.
     * @param bool   $IsMalicious Whether the file is to be voted as malicious.
     *
     * @author Patrick Kolla-ten Venne
     */
    public function __construct(string $TheDomain, bool $IsMalicious)
    {
        parent::__construct('domains', $TheDomain, $IsMalicious);
    }
}
