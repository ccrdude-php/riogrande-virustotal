<?php

/**
 * Retrieve information about a domain.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */

namespace RioGrande\VirusTotal\APIv3\Domain;

use RioGrande\VirusTotal\APIv3\RequestReport;

/**
 * Retrieve information about a domain.
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @see        https://developers.virustotal.com/reference/domain-info
 * @see        https://developers.virustotal.com/reference/domains-1
 * @since      0.1.2
 */
class Report extends RequestReport
{
    /**
     * Initializes a DomainReport request.
     *
     * @param string $TheDomain The domain to get a report about.
     *
     * @author Patrick Kolla-ten Venne
     */
    public function __construct(string $TheDomain)
    {
        parent::__construct('domains', $TheDomain);
    }

    /**
     * Returns the registrar of the domain after a successful request.
     *
     * @return string
     *
     * @author Patrick Kolla-ten Venne
     */
    public function getRegistrar(): string
    {
        if (is_null($this->QueryResponse)) {
            throw new \Exception('Not a valid answer');
        }
        return $this->QueryResponse['data']['attributes']['registrar'];
    }
}
