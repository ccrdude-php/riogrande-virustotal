<?php

/**
 * Loads all required files.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/virustotal
 * @since      0.1.2
 */

namespace RioGrande\VirusTotal;

require_once __DIR__ . '/BadRequestException.php';
require_once __DIR__ . '/InvalidArgumentException.php';
require_once __DIR__ . '/NotAvailableYetException.php';
require_once __DIR__ . '/UnselectiveContentQueryException.php';
require_once __DIR__ . '/UnsupportedContentQueryException.php';
require_once __DIR__ . '/AuthenticationRequiredException.php';
require_once __DIR__ . '/UserNotActiveException.php';
require_once __DIR__ . '/WrongCredentialsException.php';
require_once __DIR__ . '/ForbiddenException.php';
require_once __DIR__ . '/NotFoundException.php';
require_once __DIR__ . '/AlreadyExistsException.php';
require_once __DIR__ . '/FailedDependencyException.php';
require_once __DIR__ . '/QuoteExceededException.php';
require_once __DIR__ . '/TooManyRequestsException.php';
require_once __DIR__ . '/TransientException.php';
require_once __DIR__ . '/DeadlineExceededException.php';
