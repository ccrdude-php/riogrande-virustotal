<?php

/**
 * Get an IP address report.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */

namespace RioGrande\VirusTotal\APIv3;

use RioGrande\VirusTotal\APIv3\Request;

/**
 * Get an IP address report.
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @see        https://developers.virustotal.com/reference/ip-info
 * @see        https://developers.virustotal.com/reference/ip-object
 * @since      0.1.2
 */
class RequestReport extends Request
{
    protected string $Path;
    protected string $Data;
 
    /**
     * Initializes a RequestReport request.
     *
     * @param string $Path         The path to query comments in.
     * @param string $Data         The data to query comments for.
     *
     * @author Patrick Kolla-ten Venne
     */
    public function __construct(string $Path, string $Data)
    {
        $this->Path = $Path;
        $this->Data = $Data;
        $sURL = $this->APIBase;
        $sURL .= "/{$this->Path}/{$this->Data}";
        $this->setURL($sURL);
    }

    /**
     * Executes the RequestReport request.
     *
     * @return bool
     *
     * @author Patrick Kolla-ten Venne
     */
    public function execute(): bool
    {
        $this->Query->setHeaders(["x-apikey: {$this->APIKey}"]);
        $this->QueryResponse = $this->Query->execute();
        if (is_null($this->QueryResponse)) {
            return false;
        }
        return true;
    }
}
