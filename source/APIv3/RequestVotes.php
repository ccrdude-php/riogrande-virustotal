<?php

/**
 * Retrieves comments.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */

namespace RioGrande\VirusTotal\APIv3;

use RioGrande\VirusTotal\APIv3\Request;

/**
 * Retrieves comments.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @see        https://developers.virustotal.com/reference/domain-info
 * @see        https://developers.virustotal.com/reference/domains-1
 * @since      0.1.2
 */
class RequestVotes extends Request
{
    protected string $Path;
    protected string $Data;

    /**
     * Initializes the RequestComments request.
     *
     * @param string $Path         The path to query comments in.
     * @param string $Data         The data to query comments for.
     *
     * @author Patrick Kolla-ten Venne
     */
    public function __construct(string $Path, string $Data)
    {
        $this->Path = $Path;
        $this->Data = $Data;
        $sURL = $this->APIBase;
        $sURL .= "/{$this->Path}/{$this->Data}/votes";
        $this->setURL($sURL);
    }

    /**
     * Executs the DomainComments request.
     *
     * @return bool
     *
     * @author Patrick Kolla-ten Venne
     */
    public function execute(): bool
    {
        $this->Query->setHeaders(["x-apikey: {$this->APIKey}"]);
        $this->QueryResponse = $this->Query->execute();
        if (is_null($this->QueryResponse)) {
            return false;
        }
        return true;
    }

    /**
     * Returns the received number of comments.
     *
     * @return int
     *
     * @author Patrick Kolla-ten Venne
     */
    public function getVoteCount(): int
    {
        if (is_null($this->QueryResponse)) {
            throw new \Exception('Not a valid answer');
        }
        if (!isset($this->QueryResponse['meta']['count'])) {
            return 0;
        }
        return intval($this->QueryResponse['meta']['count']);
    }

    /**
     * Returns if the vote calls the domain harmless.
     *
     * @param int $TheVoteIndex Index of the vote to tell about.
     *
     * @return bool
     *
     * @author Patrick Kolla-ten Venne
     */
    public function isVerdictHarmless(int $TheVoteIndex): bool
    {
        if (is_null($this->QueryResponse)) {
            throw new \Exception('Not a valid answer');
        }
        return ($this->QueryResponse['data'][$TheVoteIndex]['attributes']['verdict']
            == 'harmless');
    }

    /**
     * Returns if the vote calls the domain malicious.
     *
     * @param int $TheVoteIndex Index of the vote to tell about.
     *
     * @return bool
     *
     * @author Patrick Kolla-ten Venne
     */
    public function isVerdictMalicious(int $TheVoteIndex): bool
    {
        if (is_null($this->QueryResponse)) {
            throw new \Exception('Not a valid answer');
        }
        return ($this->QueryResponse['data'][$TheVoteIndex]['attributes']['verdict']
            == 'malicious');
    }
}
