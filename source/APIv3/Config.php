<?php

/**
 * Configuration to store API key in file systen, not repository.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */

namespace RioGrande\VirusTotal\APIv3;

/**
 * Configuration to store API key in file systen, not repository.
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */
class Config
{
    protected string $ConfigFilename;
    protected string $APIKey = '';

    /**
     * Initializes a configuration and reads from file.
     *
     * @param string $AConfigFilename Name of the file to use to read and
     *                                write the API key.
     *
     * @author Patrick Kolla-ten Venne
     */
    public function __construct(string $AConfigFilename)
    {
        $this->ConfigFilename = $AConfigFilename;
        $this->readConfig();
    }

    /**
     * Reads configuration from disk.
     *
     * @return bool If file existed and data was read.
     *
     * @author Patrick Kolla-ten Venne
     */
    protected function readConfig(): bool
    {
        if (!file_exists($this->ConfigFilename)) {
            return false;
        }
        $a = json_decode(file_get_contents($this->ConfigFilename), true);
        if (array_key_exists('apikey', $a)) {
            $this->APIKey = $a['apikey'];
        }
        return true;
    }

    /**
     * Asks for required information if running from command line.
     *
     * @return void
     *
     * @author Patrick Kolla-ten Venne
     */
    public function requestRequiredCredentialsOnCommandLine(): void
    {
        if (PHP_SAPI === 'cli') {
            if (strlen($this->APIKey) == 0) {
                echo "\nPlease specify the API Key: ";
                $input = rtrim(fgets(STDIN));
                $this->setAPIKey($input);
            }
        }
        return;
    }

    /**
     * Writes configuration to disk.
     *
     * @return void
     *
     * @author Patrick Kolla-ten Venne
     */
    protected function writeConfig(): void
    {
        $a['apikey'] = $this->APIKey;
        file_put_contents($this->ConfigFilename, json_encode($a));
    }

    /**
     * Returns the cached API key from the configuration file.
     *
     * @return string
     *
     * @author Patrick Kolla-ten Venne
     */
    public function getAPIKey(): string
    {
        return $this->APIKey;
    }

    /**
     * Sets the API key and writes configuration file to disk.
     *
     * @param string $APIKey The key to store.
     *
     * @return Config Instance of self for chaining commands.
     *
     * @author Patrick Kolla-ten Venne
     */
    public function setAPIKey(string $APIKey): Config
    {
        $this->APIKey = $APIKey;
        $this->writeConfig();
        return $this;
    }
}
