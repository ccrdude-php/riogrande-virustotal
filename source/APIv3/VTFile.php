<?php

/**
 * Meta class for file related requests.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/virustotal
 * @since      0.1.2
 */

namespace RioGrande\VirusTotal\APIv3;

use \RioGrande\VirusTotal\APIv3\File\{AddComment, AddVote, Comments, Votes, Report};

/**
 * Meta class for file related requests.
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/virustotal
 * @since      0.1.2
 */

class VTFile
{
    protected string $ID;
    protected string $APIKey;

    /**
     * Initializes the meta object to query file stuff.
     *
     * @param string $TheID ID / SHA-1 / SHA-256 of the file.
     *
     * @author Patrick Kolla-ten Venne
     */
    public function __construct(string $TheID)
    {
        $this->ID = $TheID;
    }

    /**
     * Set the value of APIKey.
     *
     * @param string $APIKey API key to pass no to VirusTotal.
     *
     * @return VTFile Instance of self to chain commands.
     */
    public function setAPIKey(string $APIKey): VTFile
    {
        $this->APIKey = $APIKey;
        return $this;
    }

    /**
     * Retrieves the report for the file identified to the contructor.
     *
     * @return FileReport|null
     *
     * Created at: 08/05/2023, 09:52:48 (Europe/Berlin)
     * @author Patrick Kolla-ten Venne
     */
    public function getReport(): ?Report
    {
        $oReport = new Report($this->ID);
        $oReport->setAPIKey($this->APIKey);
        if (!$oReport->execute()) {
            return null;
        }
        return $oReport;
    }

    /**
     * Retrieves the behaviour reports for the file identified to the
     * contructor.
     *
     * @return FileBehaviours|null
     *
     * Created at: 08/05/2023, 09:52:48 (Europe/Berlin)
     * @author Patrick Kolla-ten Venne
     */
    public function getBehaviours(): ?FileBehaviours
    {
        $oBehaviours = new FileBehaviours($this->ID);
        $oBehaviours->setAPIKey($this->APIKey);
        if (!$oBehaviours->execute()) {
            return null;
        }
        return $oBehaviours;
    }

    /**
     * Retrieves the behaviour reports for the file identified to the
     * contructor.
     *
     * @return FileBehaviourSummary|null
     *
     * Created at: 08/05/2023, 09:52:48 (Europe/Berlin)
     * @author Patrick Kolla-ten Venne
     */
    public function getBehaviourSummary(): ?FileBehaviourSummary
    {
        $oBehaviours = new FileBehaviourSummary($this->ID);
        $oBehaviours->setAPIKey($this->APIKey);
        if (!$oBehaviours->execute()) {
            return null;
        }
        return $oBehaviours;
    }

    /**
     * Retrieves the comments for the file identified to the contructor.
     *
     * @param int $AResultLimit Maximum number of results to query.
     * 
     * @return FileComments|null
     *
     * Created at: 08/05/2023, 09:52:48 (Europe/Berlin)
     * @author Patrick Kolla-ten Venne
     */
    public function getComments(int $AResultLimit = 10): ?Comments
    {
        $oComments = new Comments($this->ID, $AResultLimit);
        $oComments->setAPIKey($this->APIKey);
        if (!$oComments->execute()) {
            return null;
        }
        return $oComments;
    }

    /**
     * Adds a comment to the file.
     *
     * @param string $TheComment Text to add as a comment.
     * 
     * @return VTFile Intance of self to change commands.
     * 
     * @author Patrick Kolla-ten Venne 
     */
    public function addComment(string $TheComment): VTFile
    {
        $oAddVote = new AddComment($this->ID, $TheComment);
        $oAddVote->setAPIKey($this->APIKey);
        $oAddVote->execute();
        return $this;
    }

    /**
     * Retrieves the comments for the file identified to the contructor.
     *
     * @return FileVotes|null
     *
     * Created at: 08/05/2023, 09:52:48 (Europe/Berlin)
     * @author Patrick Kolla-ten Venne
     */
    public function getVotes(): ?Votes
    {
        $oVotes = new Votes($this->ID);
        $oVotes->setAPIKey($this->APIKey);
        if (!$oVotes->execute()) {
            return null;
        }
        return $oVotes;
    }

    public function addVote(bool $IsMalicious): VTFile
    {
        $oAddVote = new AddVote($this->ID, $IsMalicious);
        $oAddVote->setAPIKey($this->APIKey);
        $oAddVote->execute();
        return $this;
    }
}
