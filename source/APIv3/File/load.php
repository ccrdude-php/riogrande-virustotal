<?php

/**
 * Loads all required files.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/virustotal
 * @since      0.1.2
 */

namespace RioGrande\VirusTotal;

require_once __DIR__ . '/AddComment.php';
require_once __DIR__ . '/AddVote.php';
require_once __DIR__ . '/Comments.php';
require_once __DIR__ . '/Report.php';
require_once __DIR__ . '/Votes.php';
require_once __DIR__ . '/FileBehaviours.php';
require_once __DIR__ . '/FileBehaviourMitreTrees.php';
require_once __DIR__ . '/FileBehaviourSummary.php';

