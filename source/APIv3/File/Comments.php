<?php

/**
 * Retrieves file comments.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */

namespace RioGrande\VirusTotal\APIv3\File;

use RioGrande\VirusTotal\APIv3\RequestComments;

/**
 * Retrieves file comments.
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */ 
class Comments extends RequestComments
{
    /**
     * Initializes the FileComments request.
     *
     * @param string $TheID        ID / SHA-1 / SHA-256 of file to query.
     * @param int    $AResultLimit Maximum number of results.
     *
     * @author Patrick Kolla-ten Venne
     */
    public function __construct(string $TheID, int $AResultLimit = 10)
    {
        parent::__construct('files', $TheID, $AResultLimit);
    }
}
