<?php

/**
 * Retrieves votes for a file.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */

namespace RioGrande\VirusTotal\APIv3\File;

use RioGrande\VirusTotal\APIv3\RequestVotes;

/**
 * Retrieves votes for a file.
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */
class Votes extends RequestVotes
{
    /**
     * Initializes a FileVotes request.
     *
     * @param string $TheID Domain to check
     *
     * @author Patrick Kolla-ten Venne
     */
    public function __construct(string $TheID)
    {
        parent::__construct('files', $TheID);
    }
}
