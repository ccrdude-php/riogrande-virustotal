<?php

/**
 * Summarizes the behaviour of a file.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */

namespace RioGrande\VirusTotal\APIv3;

use RioGrande\VirusTotal\APIv3\Query;

/**
 * Summarizes the behaviour of a file.
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */
class FileBehaviourSummary extends Request
{
    protected string $ID;
    protected string $UninstallExpression = '/HKEY_LOCAL_MACHINE\\\\SOFTWARE'
        . '(\\\\WOW6432Node|)\\\\Microsoft\\\\Windows'
        . '\\\\CurrentVersion\\\\Uninstall\\\\([^\\\\]*)\\\\([^\\\\Z]*)$/m';

    /**
     * Initializes the FileBehaviourSummary request.
     *
     * @param string $TheID Either SHA-256, SHA-1 or MD% identifying the fle.
     *
     * @author Patrick Kolla-ten Venne
     */
    public function __construct(string $TheID)
    {
        $this->ID = $TheID;
        $this->setURL("{$this->APIBase}/files/{$this->ID}/behaviour_summary");
    }

    /**
     * Executes the FileBehaviourSummary request.
     *
     * @return bool
     *
     * @author Patrick Kolla-ten Venne
     */
    public function execute(): bool
    {
        $this->Query->setHeaders(["x-apikey: {$this->APIKey}"]);
        $this->QueryResponse = $this->Query->execute();
        if (is_null($this->QueryResponse)) {
            return false;
        }
        return true;
    }

    /**
     * Returns the uninstall entries.
     *
     * @return array Array of uninstall keys with array of values beneath.
     *
     * Path is data/registry_keys_set/<n>/<key/value>
     *
     * @see https://regex101.com/r/YUoX6Z/1
     */
    public function getUninstallValues(): array
    {
        $aKeys = array();
        $a = $this->getQueryResponse();
        if (is_array($a)) {
            if (array_key_exists('data', $a)) {
                $aData = $a['data'];
                if ((!is_null($aData))
                    && (array_key_exists('registry_keys_set', $aData))
                ) {
                    $aRegistryKeysSet = $aData['registry_keys_set'];
                    foreach ($aRegistryKeysSet as $reg) {
                        $sKey = $reg['key'];
                        preg_match_all(
                            $this->UninstallExpression,
                            $sKey,
                            $matches,
                            PREG_SET_ORDER,
                            0
                        );
                        if ((count($matches) > 0)
                            && (array_key_exists('value', $reg))
                        ) {
                            $aKeys[$matches[0][2]][$matches[0][3]] = $reg['value'];
                        }
                    }
                }
            }
        }
        return $aKeys;
    }
}
