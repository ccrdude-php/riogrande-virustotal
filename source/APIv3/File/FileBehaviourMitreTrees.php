<?php

/**
 * Returns the recorded behaviour of a file.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */

namespace RioGrande\VirusTotal\APIv3;

/**
 * Returns the recorded behaviour of a file.
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @see        https://developers.virustotal.com/reference/file-info
 * @since      0.1.2
 */
class FileBehaviourMitreTrees extends Request
{
    protected string $ID;

    /**
     * Initializes the FileBehaviourMitreTrees request.
     *
     * @param string $TheID Either SHA-256, SHA-1 or MD% identifying the fle.
     *
     * @author Patrick Kolla-ten Venne
     */
    public function __construct(string $TheID)
    {
        $this->ID = $TheID;
        $this->setURL("{$this->APIBase}/files/{$this->ID}/behaviour_mitre_trees");
    }

    /**
     * Executes the FileBehaviourMitreTrees request.
     *
     * @return bool
     *
     * @author Patrick Kolla-ten Venne
     */
    public function execute(): bool
    {
        $this->Query->setHeaders(["x-apikey: {$this->APIKey}"]);
        $this->QueryResponse = $this->Query->execute();
        if (is_null($this->QueryResponse)) {
            return false;
        }
        return true;
    }
}
