<?php

/**
 * Returns the recorded behaviour of a file.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */

namespace RioGrande\VirusTotal\APIv3;

/**
 * Returns the recorded behaviour of a file.
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @see        https://developers.virustotal.com/reference/file-info
 * @since      0.1.2
 */
class FileBehaviours extends Request
{
    protected string $ID;

    /**
     * Initializes the FileBehaviours request.
     *
     * @param string $TheID Either SHA-256, SHA-1 or MD% identifying the fle.
     *
     * @author Patrick Kolla-ten Venne
     */
    public function __construct(string $TheID)
    {
        $this->ID = $TheID;
        $this->setURL("{$this->APIBase}/files/{$this->ID}/behaviours");
    }

    /**
     * Executes the FileBehaviours request.
     *
     * @return bool
     *
     * @author Patrick Kolla-ten Venne
     */
    public function execute(): bool
    {
        $this->Query->setHeaders(["x-apikey: {$this->APIKey}"]);
        $this->QueryResponse = $this->Query->execute();
        if (is_null($this->QueryResponse)) {
            return false;
        }
        return true;
    }

    /**
     * Returns the uninstall entries.
     *
     * @return array Array of uninstall keys with array of values beneath.
     *
     * Path is data/<n>/attributes/registry_keys_set/<n>/<key/value>
     *
     * @see https://regex101.com/r/YUoX6Z/1
     */
    public function getUninstallValues(): array
    {
        $re = '/HKEY_LOCAL_MACHINE\\\\SOFTWARE(\\\\WOW6432Node|)\\\\';
        $re .= 'Microsoft\\\\Windows\\\\CurrentVersion\\\\Uninstall\\\\';
        $re .= '([^\\\\]*)\\\\([^\\\\Z]*)$/m';
        $aKeys = [];
        $a = $this->getQueryResponse();
        if (is_array($a)) {
            $aData = $a['data'];
            foreach ($aData as $aSandbox) {
                $aAttributes = $aSandbox['attributes'];
                if (array_key_exists('registry_keys_set', $aAttributes)) {
                    $aRegistryKeysSet = $aAttributes['registry_keys_set'];
                    foreach ($aRegistryKeysSet as $reg) {
                        $sKey = $reg['key'];
                        preg_match_all($re, $sKey, $matches, PREG_SET_ORDER, 0);
                        if (count($matches) > 0) {
                            if (array_key_exists('value', $reg)) {
                                $aKeys[$matches[0][2]][$matches[0][3]] = $reg['value'];
                            }
                        }
                    }
                }
            }
        }
        return $aKeys;
    }
}
