<?php

/**
 * Adds a vote to a file.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */

namespace RioGrande\VirusTotal\APIv3\File;

use RioGrande\VirusTotal\APIv3\RequestAddVote;

/**
 * Adds a vote to a file.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */
class AddVote extends RequestAddVote
{
    /**
     * Initialitzes a FileAddVote request.
     *
     * @param string $TheID       ID / SHA-1 / SHA-256 of the file to vote on.
     * @param bool   $IsMalicious Whether to file should be votes malicious.
     *
     * @author Patrick Kolla-ten Venne
     */
    public function __construct(string $TheID, bool $IsMalicious)
    {
        parent::__construct('files', $TheID, $IsMalicious);
    }
}
