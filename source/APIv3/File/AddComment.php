<?php

/**
 * Adds a Comment for a file.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */

namespace RioGrande\VirusTotal\APIv3\File;

use RioGrande\VirusTotal\APIv3\RequestAddComment;

/**
 * Adds a Comment for a file.
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */
class AddComment extends RequestAddComment
{
   /**
     * Initializes a FileAddComment request.
     *
     * @param string $Hash      The has of the file.
     * @param string $Comment   Whether the file is to be Commentd as malicious.
     *
     * @author Patrick Kolla-ten Venne
     */
    public function __construct(string $Hash, string $Comment)
    {
        parent::__construct('files', $Hash, $Comment);
    }
}
