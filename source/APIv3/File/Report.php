<?php

/**
 * Summarizes a file.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */

namespace RioGrande\VirusTotal\APIv3\File;

use RioGrande\VirusTotal\APIv3\RequestReport;

/**
 * Summarizes a file.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @see        https://developers.virustotal.com/reference/file-info
 * @since      0.1.2
 */
class Report extends RequestReport
{
    /**
     * Initializes a FileReport request.
     *
     * @param string $TheID Either SHA-256, SHA-1 or MD5 identifying the fle.
     *
     * @author Patrick Kolla-ten Venne
     */
    public function __construct(string $TheID)
    {
        parent::__construct('files', $TheID);
    }

    /**
     * Returns the extension of a after a successful request.
     *
     * @return string
     *
     * @author Patrick Kolla-ten Venne
     */
    public function getFileTypeExtension(): string
    {
        if (is_null($this->QueryResponse)) {
            throw new \Exception('Not a valid answer');
        }
        return $this->QueryResponse['data']['attributes']['type_extension'];
    }

    /**
     * Returns the type of a result after a successful request.
     *
     * @return string
     *
     * @author Patrick Kolla-ten Venne
     */
    public function getFileTypeDescription(): string
    {
        if (is_null($this->QueryResponse)) {
            throw new \Exception('Not a valid answer');
        }
        return $this->QueryResponse['data']['attributes']['type_description'];
    }
}
