<?php

/**
 * Meta class for IP address related requests.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/virustotal
 * @since      0.1.3
 */

namespace RioGrande\VirusTotal\APIv3;

use \RioGrande\VirusTotal\APIv3\IPAddress\{AddComment, AddVote, Comments, ObjectDescriptors, Objects, Report, Votes};

/**
 * Meta class for address related requests.
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/virustotal
 * @since      0.1.3
 */

class VTIPAddress
{
    protected string $IPAddress;
    protected string $APIKey;

    /**
     * Initializes the meta object to query IP address stuff.
     *
     * @param string $TheIPAddress IP address.
     *
     * @author Patrick Kolla-ten Venne
     */
    public function __construct(string $TheIPAddress)
    {
        $this->IPAddress = $TheIPAddress;
    }

    /**
     * Set the value of APIKey.
     *
     * @param string $APIKey API key to pass no to VirusTotal.
     *
     * @return VTIPAddress Instance of self to chain commands.
     */
    public function setAPIKey(string $APIKey): VTIPAddress
    {
        $this->APIKey = $APIKey;
        return $this;
    }

    /**
     * Retrieves the report for the IP address identified to the contructor.
     *
     * @return Report|null
     *
     * Created at: 08/05/2023, 09:52:48 (Europe/Berlin)
     * @author Patrick Kolla-ten Venne
     */
    public function getReport(): ?Report
    {
        $oReport = new Report($this->IPAddress);
        $oReport->setAPIKey($this->APIKey);
        if (!$oReport->execute()) {
            return null;
        }
        return $oReport;
    }

    /**
     * Retrieves the comments for the IP addreess identified to the contructor.
     *
     * @param int $AResultLimit Maximum number of results to query.

     * @return Comments|null
     *
     * Created at: 08/05/2023, 09:52:48 (Europe/Berlin)
     * @author Patrick Kolla-ten Venne
     */
    public function getComments(int $AResultLimit = 10): ?Comments
    {
        $oComments = new Comments($this->IPAddress, $AResultLimit);
        $oComments->setAPIKey($this->APIKey);
        if (!$oComments->execute()) {
            return null;
        }
        return $oComments;
    }

    /**
     * Adds a comment to the IP address.
     * Created at: 08/05/2023, 21:41:23 (Europe/Berlin)
     *
     * @param string $TheComment Text to add as a comment..
     * 
     * @return VTIPAddress Intance of self to change commands.
     * 
     * @author Patrick Kolla-ten Venne 
     */
    public function addComment(string $TheComment): VTIPAddress
    {
        $oAddVote = new AddComment($this->IPAddress, $TheComment);
        $oAddVote->setAPIKey($this->APIKey);
        $oAddVote->execute();
        return $this;
    }

    /**
     * Retrieves the comments for the file identified to the contructor.
     *
     * @return Votes|null
     *
     * Created at: 08/05/2023, 09:52:48 (Europe/Berlin)
     * @author Patrick Kolla-ten Venne
     */
    public function getVotes(): ?Votes
    {
        $oVotes = new Votes($this->IPAddress);
        $oVotes->setAPIKey($this->APIKey);
        if (!$oVotes->execute()) {
            return null;
        }
        return $oVotes;
    }

    /**
     * Adds a verdict to the IP address.
     * Created at: 08/05/2023, 21:41:23 (Europe/Berlin)
     *
     * @param bool $IsMalicious The verdict to pass.
     * 
     * @return VTIPAddress Intance of self to change commands.
     * 
     * @author Patrick Kolla-ten Venne 
     */
    public function addVote(bool $IsMalicious): VTIPAddress
    {
        $oAddVote = new AddVote($this->IPAddress, $IsMalicious);
        $oAddVote->setAPIKey($this->APIKey);
        $oAddVote->execute();
        return $this;
    }

    /**
     * Retrieves the objects for the IP addreess identified to the contructor.
     * Created at: 08/05/2023, 09:52:48 (Europe/Berlin)
     *
     * @param string $Relationship Type of object to query.
     * @param int    $AResultLimit Maximum number of results to query.
     * 
     * @return Objects|null
     *
     * @author Patrick Kolla-ten Venne
     */
    public function getObjects(string $Relationship, int $AResultLimit = 10): ?Objects
    {
        $oObjects = new Objects($this->IPAddress, $Relationship, $AResultLimit);
        $oObjects->setAPIKey($this->APIKey);
        if (!$oObjects->execute()) {
            return null;
        }
        return $oObjects;
    }

    /**
     * Retrieves the object descriptors for the IP addreess identified to the contructor.
     * Created at: 08/05/2023, 09:52:48 (Europe/Berlin)
     *
     * @param string $Relationship Type of object to query.
     * @param int    $AResultLimit Maximum number of results to query.
     * 
     * @return ObjectDescriptors|null
     *
     * @author Patrick Kolla-ten Venne
     */
    public function getObjectDescriptors(string $Relationship, int $AResultLimit = 10): ?ObjectDescriptors
    {
        $oObjectDescriptors = new ObjectDescriptors($this->IPAddress, $Relationship, $AResultLimit);
        $oObjectDescriptors->setAPIKey($this->APIKey);
        if (!$oObjectDescriptors->execute()) {
            return null;
        }
        return $oObjectDescriptors;
    }
}
