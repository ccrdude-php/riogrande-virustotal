<?php

/**
 * Most simple test of functionality, using code directly.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */

namespace RioGrande\VirusTotal\APIv3;

require_once __DIR__ . '/source/load.php';

use RioGrande\VirusTotal\APIv3\{Config, FileBehaviourSummary, DomainAddVote, DomainComments, IPAddComment};

$sResource = '8c8fbcf80f0484b48a07bd20e512b103969992dbf81b6588832b08205e3a1b43';

$cfg = new Config(__DIR__ . '/config.json');
$cfg->requestRequiredCredentialsOnCommandLine();

/*
$report = new FileBehaviourSummary($sResource);
$report->setAPIKey($cfg->getAPIKey());
$report->execute();

$aKeys = array();
print_r($report->getUninstallValues());
*/

/*
$report = new DomainAddVote('spybot.de', false);
$report->setAPIKey($cfg->getAPIKey());
$report->execute();
*/

$report = new Domain\Votes('ccrdude.net');
$report->setAPIKey($cfg->getAPIKey());
$report->setAPIKey('caffee');
$b = $report->execute();
var_dump($b);
var_dump($report->getQueryResponse());
var_dump($report->getDebugOutput());


/*
$newComment = new IPAddComment('144.76.80.198', 'Server owned by software developer CCRDude');
$newComment->setAPIKey($cfg->getAPIKey());
$b = $newComment->execute();
var_dump($b);
var_dump($newComment->getQueryResponse());
*/