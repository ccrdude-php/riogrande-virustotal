<?php

/**
 * Most simple test of functionality, using phar file.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage VirusTotal
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-virustotal
 * @since      0.1.2
 */

namespace RioGrande\VirusTotal;

require_once './tools/riogrande-virustotal-0.1.2.phar';

$cfg = new \RioGrande\VirusTotal\APIv3\Config(__DIR__ . '/config.json');
$cfg->requestRequiredCredentialsOnCommandLine();

$sResource = '8c8fbcf80f0484b48a07bd20e512b103969992dbf81b6588832b08205e3a1b43';
$report = new \RioGrande\VirusTotal\APIv3\FileBehaviours($sResource);
$report->setAPIKey($cfg->getAPIKey());
$report->execute();
print_r($report->getUninstallValues());

$file = new \RioGrande\VirusTotal\APIv3\VTFile($sResource);
$file->setAPIKey($cfg->getAPIKey());
$behaviours = $file->getBehaviours();
print_r($behaviours->getUninstallValues());
